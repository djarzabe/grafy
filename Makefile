CPPFLAGS= -c -g -std=c++11 -iquote inc -Wall -pedantic

algorytm_Djikstry: obj obj/main.o obj/Graf.o obj/MacierzGraf.o
	g++ -std=c++11 -Wall -pedantic -o algorytm_Djikstry obj/main.o obj/Graf.o obj/MacierzGraf.o

obj/main.o: src/main.cpp inc/Lista.hh inc/Kolejka.hh inc/Graf.hh inc/MacierzGraf.hh
	g++ ${CPPFLAGS} -o obj/main.o src/main.cpp

obj/Graf.o: src/Graf.cpp inc/Lista.hh inc/Kolejka.hh inc/Graf.hh
	g++ ${CPPFLAGS} -o obj/Graf.o src/Graf.cpp

obj/MacierzGraf.o: src/MacierzGraf.cpp inc/Lista.hh inc/Kolejka.hh inc/MacierzGraf.hh
	g++ ${CPPFLAGS} -o obj/MacierzGraf.o src/MacierzGraf.cpp

clean:
	rm -f obj/*.o algorytm_Djikstry
