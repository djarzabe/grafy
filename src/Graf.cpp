#include "Graf.hh"

Lista<Graf::Krawedz*> Graf::incydentneKrawedzie(const Wierzcholek & w)const{
  return w.incydentne;
}

Lista<Graf::Krawedz> Graf::KrawedzieGrafu()const{
  return Lista_Krawedzi;
}

Lista<Graf::Wierzcholek> Graf::WierzcholkiGrafu()const{
  return Lista_Wierzcholkow;
}
  
void Graf::nowyElement(Wierzcholek &w, int nowa_wartosc){
  w.element=nowa_wartosc;
}

void Graf::nowyElement(Krawedz &k, int nowa_wartosc){
  k.waga=nowa_wartosc;
}

bool Graf::sasiedzi(const Wierzcholek& v,const Wierzcholek& w){
  Lista<Wierzcholek>::Iterator ww=znajdzWierzcholek(w);
  Lista<Wierzcholek>::Iterator vv=znajdzWierzcholek(v);
  Lista<Krawedz*>::Iterator p=(*vv).incydentne.poczatek();
  while(p!=(*vv).incydentne.koniec()){if(((*p)->A)==&(*ww)||((*p)->B)==&(*ww)){return true;} ++p;}
  return false;
}

Graf::ParaWierzcholkow Graf::konceKrawedzi(const Krawedz& k){
 return ParaWierzcholkow (*(k.A),*(k.B));
}

Graf::Wierzcholek Graf::naprzeciwko(const Wierzcholek& w,const Krawedz& k){
  if((*(k.A))==w){return *(k.B);}
  else if((*(k.B))==w){return *(k.A);}
  else{std::cerr<<"Nie ma takiej pary wierzcholka i krawedzi"<<std::endl;
    return w;}
}

void Graf::dodajWierzcholek(int elem){
  Wierzcholek w(elem);
  Lista_Wierzcholkow.dodajTyl(w);
}

void Graf::dodajWierzcholek(const Wierzcholek &w){
  Lista_Wierzcholkow.dodajTyl(w);
}

void Graf::dodajKrawedz(Krawedz &k){
  Lista_Krawedzi.dodajTyl(k);
  Lista<Krawedz>::Iterator i=Lista_Krawedzi.koniec();--i;
  Krawedz* wsk_k=&(*i);
  k.A->incydentne.dodajTyl(wsk_k);
  k.B->incydentne.dodajTyl(wsk_k);
  
}


void Graf::dodajKrawedz(Wierzcholek &v,Wierzcholek& w,int elem){
  Krawedz k(&v,&w,elem);
  Lista_Krawedzi.dodajTyl(k);
  Lista<Krawedz>::Iterator i=Lista_Krawedzi.koniec();--i;
  Krawedz* wsk_k=&(*i);
  v.incydentne.dodajTyl(wsk_k);
  w.incydentne.dodajTyl(wsk_k);
  
}

void Graf::dodajKrawedz(int w1_elem, int w2_elem, int wartosc){
  Lista<Wierzcholek>::Iterator w1=znajdzWierzcholek(Wierzcholek(w1_elem)),
    w2=znajdzWierzcholek(Wierzcholek(w2_elem));
  dodajKrawedz((*w1),(*w2),wartosc);
}
   


bool Graf::szukajWierzcholka(const Wierzcholek& w){
  return Lista_Wierzcholkow.znajdz(w)!=Lista_Wierzcholkow.koniec();
}

Lista<Graf::Wierzcholek>::Iterator Graf::znajdzWierzcholek(const Wierzcholek& w){
  return Lista_Wierzcholkow.znajdz(w);
}

Lista<Graf::Krawedz>::Iterator Graf::znajdzKrawedz(const Krawedz& k){
  return Lista_Krawedzi.znajdz(k);
}

void Graf::usunKrawedz(Krawedz& k){
  Lista<Krawedz>::Iterator p=Lista_Krawedzi.znajdz(k);
  Lista<Krawedz*>::Iterator i=(*p).A->incydentne.znajdz(&(*p));
  (*p).A->incydentne.usun(i);
  i=(*p).B->incydentne.znajdz(&(*p));
  (*p).B->incydentne.usun(i);
  Lista_Krawedzi.usun(p);
}

void Graf::usunWierzcholek(Wierzcholek &w){
  Lista<Wierzcholek>::Iterator p=Lista_Wierzcholkow.znajdz(w);
  Lista<Krawedz*>::Iterator i=w.incydentne.poczatek();
  while(i!=w.incydentne.koniec()){
    usunKrawedz(*(*i));
    ++i;
  }
  Lista_Wierzcholkow.usun(p);
}

void Graf::Wierzcholek::ustaw_droge(Wierzcholek& w,Krawedz& k){
  droga=w.droga+k.waga;
  Lista<int>::Iterator test=w.trasa.poczatek();
  Lista<int>test1;
  trasa=test1;
  while (test!=w.trasa.koniec()){trasa.dodajTyl(*test);++test;}
  trasa.dodajTyl(w.element);
}


void Graf::znajdz_sciezke(const Wierzcholek& start){
  Kolejka<Graf::Wierzcholek,1001> k;
  unsigned int i=1;
  Lista<Wierzcholek>::Iterator p=(WierzcholkiGrafu()).poczatek();
  for(;p!=(WierzcholkiGrafu()).koniec();++p){
    if(*p==start){(*p).droga=(0);}
    else{(*p).droga=(oo);}
    (*p).kolejka=i;
    ++i;
    k.dodaj((*p));
  }
  while(!k.pusta()){
    Wierzcholek* u=k.usun_min();
    Lista<Krawedz*>::Iterator p=((incydentneKrawedzie(*u)).poczatek());
    for(;p!=(incydentneKrawedzie(*u)).koniec();++p){
      Wierzcholek v=naprzeciwko(*u,*(*p));
      Lista<Wierzcholek>::Iterator ww=znajdzWierzcholek(v);
      if((*ww).droga>(u->droga+(*p)->waga)){
	(*ww).ustaw_droge(*u,*(*p));
	k.po_zamianie_klucza((*ww).kolejka);
      }
    }
  }
}

