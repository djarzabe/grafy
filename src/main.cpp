#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <fstream>
#include "Lista.hh"
#include "Kolejka.hh"
#include "Graf.hh"
#include "MacierzGraf.hh"
using namespace std;

template<>
void Lista<MacierzGraf::MWierzcholek>::wyswietl() const{
  Iterator p=poczatek();
  while(p!=koniec()){std::cout<<(*p).element<<" "<<(*p).droga<<" "<<(*p).numer<<"   "; ++p;}
  std::cout<<std::endl<<std::endl;
}

template<>
void Lista<MacierzGraf::MKrawedz>::wyswietl() const{
  Iterator p=poczatek();
  while(p!=koniec()){
    std::cout<<(*p).A->element<<" "<<(*p).B->element<<" "<<(*p).waga<<" "<<std::endl;
    ++p;}
  std::cout<<std::endl;
}

template<>
void Lista<MacierzGraf::MKrawedz*>::wyswietl()const{
  Iterator p=poczatek();
  while(p!=koniec()){
    std::cout<<(*p)->A->element<<" "<<(*p)->B->element<<" "<<(*p)->waga<<" "<<std::endl;
    ++p;}
   std::cout<<std::endl;
}



ostream& operator << (ostream &Strm, const Lista<int> &Listewka){
  Lista<int>::Iterator i=Listewka.poczatek();
  while(i!=Listewka.koniec()){Strm<<(*i)<<" ";++i;}
  return Strm;
}
template<>
void Lista<Graf::Wierzcholek>::wyswietl() const{
  Iterator p=poczatek();
  while(p!=koniec()){std::cout<<(*p).element<<" "<<(*p).droga<<"   "; ++p;}
  std::cout<<std::endl<<std::endl;
}
template<>
void Lista<Graf::Krawedz>::wyswietl() const{
  Iterator p=poczatek();
  while(p!=koniec()){
    std::cout<<(*p).A->element<<" "<<(*p).B->element<<" "<<(*p).waga<<" "<<std::endl;
    ++p;}
  std::cout<<std::endl;
}

template<>
void Lista<Graf::Krawedz*>::wyswietl()const{
  Iterator p=poczatek();
  while(p!=koniec()){
    std::cout<<(*p)->A->element<<" "<<(*p)->B->element<<" "<<(*p)->waga<<" "<<std::endl;
    ++p;}
  std::cout<<std::endl;
}


int main(){
  srand(time(NULL));
  clock_t start, stop;
  double czas1=0,czas2=0;
  int liczba;
  int ilosc_grafow=100;
  unsigned int rozmiarGrafu[]={10,50,100,500,1000};
  double gestoscGrafu[]={0.25,0.5,0.75,1};
  unsigned  int maxKrawedzi;
  unsigned int licznikWierzcholkow;
  unsigned int licznikKrawedzi;

  //**********************LISTA SASIEDZTWA*********************
  
  ofstream lista_czasy ("lista_czasy");
 
  for(unsigned int wymiar : rozmiarGrafu){
    for(double gestosc : gestoscGrafu){
      for(int gr=0;gr<ilosc_grafow;++gr){
	Graf G;
	int *tab=new int[wymiar];
	maxKrawedzi=wymiar*(wymiar-1)*gestosc/2;
	licznikWierzcholkow=0;
	licznikKrawedzi=0;
	while(licznikWierzcholkow<wymiar){
	  liczba=rand()%10000;
	  if(!G.szukajWierzcholka(liczba)){
	    G.dodajWierzcholek(Graf::Wierzcholek(liczba));
	    tab[licznikWierzcholkow]=liczba;
	    ++licznikWierzcholkow;
	  }
	}
	if(gestosc!=1){
	  for(unsigned int i=0;i+1<wymiar && licznikKrawedzi<maxKrawedzi;++i){
	    liczba=rand()%1000;
	    G.dodajKrawedz(tab[i],tab[i+1],liczba);
	    ++licznikKrawedzi;
	  }
	  while(licznikKrawedzi<maxKrawedzi){
	    unsigned int i=rand()%wymiar;
	    unsigned int j;
	    do{
	      j=rand()%wymiar;
	    }while(j==i);
	    if(!G.sasiedzi(tab[i],tab[j])){
	      liczba=rand()%1000;
	      G.dodajKrawedz(tab[i],tab[j],liczba);
	      ++licznikKrawedzi;
	    }
	  }
	}
	else{
	  for(unsigned int i=0;i<wymiar && licznikKrawedzi<maxKrawedzi;++i){
	    for(unsigned int j=1;(i+j)<wymiar && licznikKrawedzi<maxKrawedzi;++j){
	      liczba=rand()%1000;
	      G.dodajKrawedz(tab[i],tab[i+j],liczba);
	      ++licznikKrawedzi;
	    }
	  }
	}
	start = clock();
	G.znajdz_sciezke(Graf::Wierzcholek(tab[0]));
	stop = clock();
	czas1+=(double)(stop-start)/CLOCKS_PER_SEC;
	delete [] tab;
      }
      czas1/=ilosc_grafow;
      lista_czasy<<wymiar<<" "<<gestosc<<" "<<czas1<<endl;
      czas1=0;
    }
    
  }
  lista_czasy.close();
  
  
  
  //******************MACIERZ SASIEDZTWA****************
  ofstream macierz_czasy ("macierz_czasy");
  for(unsigned int wymiar : rozmiarGrafu){
    for(double gestosc : gestoscGrafu){
      for(int gr=0;gr<ilosc_grafow;++gr){
	MacierzGraf M;
	int *tab=new int[wymiar];
	maxKrawedzi=wymiar*(wymiar-1)*gestosc/2;
	licznikWierzcholkow=0;
	licznikKrawedzi=0;
	while(licznikWierzcholkow<wymiar){
	  liczba=rand()%10000;
	  if(!M.szukajWierzcholka(liczba)){
	    M.dodajWierzcholek(liczba);
	    tab[licznikWierzcholkow]=liczba;
	    ++licznikWierzcholkow;
	  }
	}
	if(gestosc!=1){
	  for(unsigned int i=0;i+1<wymiar && licznikKrawedzi<maxKrawedzi;++i){
	    liczba=rand()%1000;
	    M.dodajKrawedz(tab[i],tab[i+1],liczba);
	    ++licznikKrawedzi;
	  }
	  while(licznikKrawedzi<maxKrawedzi){
	    unsigned int i=rand()%wymiar;
	    unsigned int j;
	    do{
	      j=rand()%wymiar;
	    }while(j==i);
	    if(!M.sasiedzi(tab[i],tab[j])){
	      liczba=rand()%1000;
	      M.dodajKrawedz(tab[i],tab[j],liczba);
	      ++licznikKrawedzi;
	    }
	  }
	}
	else{
	  for(unsigned int i=0;i<wymiar && licznikKrawedzi<maxKrawedzi;++i){
	    for(unsigned int j=1;(i+j)<wymiar && licznikKrawedzi<maxKrawedzi;++j){
	      liczba=rand()%10000;
	      M.dodajKrawedz(tab[i],tab[i+j],liczba);
	      ++licznikKrawedzi;
	    }
	  }
	}
	start = clock();
	M.znajdz_sciezke(MacierzGraf::MWierzcholek(tab[0]));
	stop = clock();
	czas2+=(double)(stop-start)/CLOCKS_PER_SEC;
	delete [] tab;
      }
      czas2/=ilosc_grafow;
      macierz_czasy<<wymiar<<" "<<gestosc<<" "<<czas2<<endl;
      czas2=0;
   }
  }
  macierz_czasy.close();
  
  
  //czytanie z pliku/konsoli i zapis do pliku 
  Graf G;  MacierzGraf M;
  int ilosc_wierzcholkow;
  int ilosc_krawedzi;
  int wierzcholek_startowy;
  int wierzcholek_poczatkowy;
  int wierzcholek_koncowy;
  int waga;
  cin>>ilosc_wierzcholkow>>ilosc_krawedzi>>wierzcholek_startowy;
  for(int i=0;i<ilosc_krawedzi;++i){
    //Lista sasiedztwa
    cin>>wierzcholek_poczatkowy>>wierzcholek_koncowy>>waga;
    if(!G.szukajWierzcholka(wierzcholek_poczatkowy)){
      G.dodajWierzcholek(wierzcholek_poczatkowy);
    }
    if(!G.szukajWierzcholka(wierzcholek_koncowy)){
      G.dodajWierzcholek(wierzcholek_koncowy);
    }
    G.dodajKrawedz(wierzcholek_poczatkowy,wierzcholek_koncowy,waga);
    //Macierz sasiedztwa
    if(!M.szukajWierzcholka(wierzcholek_poczatkowy)){
      M.dodajWierzcholek(wierzcholek_poczatkowy);
    }
    if(!M.szukajWierzcholka(wierzcholek_koncowy)){
      M.dodajWierzcholek(wierzcholek_koncowy);
    }
    M.dodajKrawedz(wierzcholek_poczatkowy,wierzcholek_koncowy,waga);
  }
  //Lista sasiedztwa
  G.znajdz_sciezke(Graf::Wierzcholek(wierzcholek_startowy));
  ofstream DjikstraL ("DjikstraL");
  Lista<Graf::Wierzcholek>::Iterator l=(G.WierzcholkiGrafu()).poczatek();
  for(;l!=(G.WierzcholkiGrafu()).koniec();++l){
    DjikstraL<<(*l).element<<" "<<(*l).droga<<" | "<< (*l).trasa<<std::endl;
  }
  DjikstraL.close();
  //Macierz sasiedztwa
  M.znajdz_sciezke(MacierzGraf::MWierzcholek(wierzcholek_startowy));
  ofstream DjikstraM ("DjikstraM");
  Lista<MacierzGraf::MWierzcholek>::Iterator m=(M.WierzcholkiGrafu()).poczatek();
  for(;m!=(M.WierzcholkiGrafu()).koniec();++m){
    DjikstraM<<(*m).element<<" "<<(*m).droga <<" | "<<(*m).trasa <<std::endl;
  }
  DjikstraM.close();
  
}

