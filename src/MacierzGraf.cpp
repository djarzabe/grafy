#include "MacierzGraf.hh"

MacierzGraf::MacierzGraf(){
  ostatni=-1;
  for (int i =0;i<MAX;++i){
    for(int j=0;j<MAX;++j){
      Incydentne[i][j]=nullptr;
    }
  }
}

MacierzGraf::MParaWierzcholkow MacierzGraf::konceKrawedzi(const MKrawedz& k){
 return MParaWierzcholkow (*(k.A),*(k.B));
}

bool MacierzGraf::sasiedzi(const MWierzcholek& v,const MWierzcholek& w){
  Lista<MWierzcholek>::Iterator w1=znajdzWierzcholek(v),
    w2=znajdzWierzcholek(w);
  return Incydentne[(*w1).numer][(*w2).numer]!=nullptr;
}

  
Lista<MacierzGraf::MKrawedz> MacierzGraf::KrawedzieGrafu()const{
  return Lista_Krawedzi;
}

Lista<MacierzGraf::MWierzcholek> MacierzGraf::WierzcholkiGrafu()const{
  return Lista_Wierzcholkow;
}

Lista<MacierzGraf::MKrawedz*> MacierzGraf::incydentneKrawedzie( const MWierzcholek& w)const{
  Lista<MKrawedz*> sasiedzi;
  for (int i =0;i<=ostatni;++i){
    if(Incydentne[w.numer][i]!=nullptr){
    sasiedzi.dodajTyl(Incydentne[w.numer][i]);}
  }
  return sasiedzi;    
}

void MacierzGraf::dodajWierzcholek(int elem){
  MWierzcholek w(elem);
  dodajWierzcholek(w);
}

void MacierzGraf::dodajWierzcholek(MWierzcholek &w){
  ++ostatni;
  w.numer=ostatni;
  Lista_Wierzcholkow.dodajTyl(w);
}

void MacierzGraf::dodajKrawedz(MWierzcholek &v,MWierzcholek& w,int elem){
  MKrawedz k(&v,&w,elem);
  Lista_Krawedzi.dodajTyl(k);
  Lista<MKrawedz>::Iterator i=Lista_Krawedzi.koniec();--i;
  MKrawedz* wsk_k=&(*i);
  Incydentne[v.numer][w.numer]=wsk_k;
  Incydentne[w.numer][v.numer]=wsk_k;
  
}

void MacierzGraf::dodajKrawedz(int w1_elem, int w2_elem, int wartosc){
  Lista<MWierzcholek>::Iterator w1=znajdzWierzcholek(MWierzcholek(w1_elem)),
    w2=znajdzWierzcholek(MWierzcholek(w2_elem));
  dodajKrawedz((*w1),(*w2),wartosc);
}

void MacierzGraf::usunKrawedz(MKrawedz& k){
  Lista<MKrawedz>::Iterator p=Lista_Krawedzi.znajdz(k);
  Incydentne[(*p).A->numer][(*p).B->numer]=nullptr;
  Incydentne[(*p).B->numer][(*p).A->numer]=nullptr;
  Lista_Krawedzi.usun(p);
}

void MacierzGraf::usunWierzcholek(MWierzcholek &w){
  Lista<MWierzcholek>::Iterator p=Lista_Wierzcholkow.znajdz(w);
  Lista<MKrawedz*> sasiedzi=incydentneKrawedzie(*p);
  Lista<MKrawedz*>::Iterator i=sasiedzi.poczatek();
  while(i!=sasiedzi.koniec()){
    usunKrawedz(*(*i));
    ++i;
  }
  if((*p).numer==ostatni){--ostatni;}
  Lista_Wierzcholkow.usun(p);
}

Lista<MacierzGraf::MWierzcholek>::Iterator MacierzGraf::znajdzWierzcholek(const MWierzcholek& w){
  return Lista_Wierzcholkow.znajdz(w);
}

MacierzGraf::MWierzcholek MacierzGraf::naprzeciwko(const MWierzcholek& w,const MKrawedz& k){
  if((*(k.A))==w){return *(k.B);}
  else if((*(k.B))==w){return *(k.A);}
  else{std::cerr<<"Nie ma takiej pary wierzcholka i krawedzi"<<std::endl;
    return w;}
}

void MacierzGraf::MWierzcholek::ustaw_droge(MWierzcholek& w,MKrawedz& k){
  droga=w.droga+k.waga;
  Lista<int>::Iterator test=w.trasa.poczatek();
  Lista<int>test1;
  trasa=test1;
  while (test!=w.trasa.koniec()){trasa.dodajTyl(*test);++test;}
  trasa.dodajTyl(w.element);
}

void MacierzGraf::znajdz_sciezke(const MWierzcholek& start){

  Kolejka<MacierzGraf::MWierzcholek,1001> k;
  unsigned int i=1;
  Lista<MWierzcholek>::Iterator p=(WierzcholkiGrafu()).poczatek();
  for(;p!=(WierzcholkiGrafu()).koniec();++p){
    if(*p==start){(*p).droga=(0);}
    else{(*p).droga=(oo);}
    (*p).kolejka=i;
    ++i;
    k.dodaj((*p));
  }
  while(!k.pusta()){
    MWierzcholek* u=k.usun_min();
    Lista<MKrawedz*>test=incydentneKrawedzie(*u);
    Lista<MKrawedz*>::Iterator p=test.poczatek();
    for(;p!=test.koniec();++p){
      MWierzcholek v=naprzeciwko(*u,*(*p));
      Lista<MWierzcholek>::Iterator ww=znajdzWierzcholek(v);
      if((*ww).droga>(u->droga+(*p)->waga)){
	(*ww).ustaw_droge(*u,*(*p));
	k.po_zamianie_klucza((*ww).kolejka);
      }
    }
  }
}

bool MacierzGraf::szukajWierzcholka(const MWierzcholek& w){
  return Lista_Wierzcholkow.znajdz(w)!=Lista_Wierzcholkow.koniec();
}

