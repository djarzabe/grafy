#ifndef LISTA_HH
#define LISTA_HH
#include <iostream>
template<typename Elem>
class Lista{

  struct Wezel{
    Elem element;
    Wezel* poprzedni;
    Wezel* nastepny;
  };

  unsigned int n;
  Wezel* glowa;
  Wezel* ogon;

public:

  class Iterator{
    Wezel* w;
    Iterator(Wezel* u);
  public:
    Elem& operator * ();
    bool operator == (const Iterator& p) const;
    bool operator != (const Iterator& p) const;
    Iterator& operator ++ ();
    Iterator & operator -- ();
    friend class Lista;
  };

  Lista();
  int rozmiar() const;
  bool pusta() const;
  Iterator poczatek() const;
  Iterator koniec() const;
  void dodajPrzod(const Elem& e);
  void dodajTyl(const Elem& e);
  void dodaj(const Iterator& p,const Elem& e);
  void usunPrzod();
  void usunTyl();
  void usun(const Iterator& p);
  void wyswietl() const;
  Iterator znajdz(const Elem& e);
};


template<typename Elem>
typename Lista<Elem>::Iterator Lista<Elem>::znajdz(const Elem &e){
  typename Lista<Elem>::Iterator p=Lista<Elem>::poczatek();
  while(p!=Lista<Elem>::koniec()){if((*p)==e){return p;} ++p;}
  return p;
}


template<typename Elem>
Lista<Elem>::Lista(){
  n = 0;
  glowa =new Wezel;
  ogon =new Wezel;
  glowa -> nastepny = ogon;
  ogon -> poprzedni = glowa;
}

template<typename Elem>
void Lista<Elem>::wyswietl() const{
  Iterator p=poczatek();
  while(p!=koniec()){std::cout<<*p<<" "; ++p;}
  std::cout<<std::endl;
}



template<typename Elem>
int Lista<Elem>::rozmiar() const
{return n;}

template<typename Elem>
bool Lista<Elem>::pusta() const
{return(n == 0);}

template<typename Elem>
typename Lista<Elem>::Iterator Lista<Elem>::poczatek() const
{return Iterator(glowa->nastepny);}

template<typename Elem>
typename Lista<Elem>::Iterator Lista<Elem>::koniec() const
{return Iterator(ogon);}

template<typename Elem>
void Lista<Elem>::dodaj(const Lista<Elem>::Iterator& p,const Elem& e){
  Wezel* w = p.w;
  Wezel* u = w->poprzedni;
  Wezel* v = new Wezel;
  v->element = e;
  v->nastepny = w;
  w->poprzedni = v;
  v->poprzedni = u; u->nastepny = v;
  ++n;
}

template<typename Elem>
void Lista<Elem>::dodajPrzod(const Elem& e)
{dodaj(poczatek(), e);}

template<typename Elem>
void Lista<Elem>::dodajTyl(const Elem& e)
{dodaj(koniec(), e);}

template<typename Elem>
void Lista<Elem>::usun(const Iterator& p){
  Wezel* v = p.w;
  Wezel* w = v->nastepny;
  Wezel* u = v->poprzedni;
  u->nastepny = w; w->poprzedni = u;
  delete v;
  --n;
}

template<typename Elem>
void Lista<Elem>::usunPrzod()
{usun(poczatek());}

template<typename Elem>
void Lista<Elem>::usunTyl()
{usun(--koniec());}

template<typename Elem>
Lista<Elem>::Iterator::Iterator(Wezel* u)
{w = u;}

template<typename Elem>
Elem& Lista<Elem>::Iterator::operator * ()
{return w->element;}

template<typename Elem>
bool Lista<Elem>::Iterator::operator == (const Iterator& p) const
{return w == p.w;}

template<typename Elem>
bool Lista<Elem>::Iterator::operator != (const Iterator& p) const
{return w != p.w;}

template<typename Elem>
typename Lista<Elem>::Iterator& Lista<Elem>::Iterator::operator ++ ()
{w = w->nastepny; return *this;}

template<typename Elem>
typename Lista<Elem>::Iterator& Lista<Elem>::Iterator::operator -- ()
{w = w->poprzedni; return *this;}

#endif
