#ifndef KOLEJKA_HH
#define KOLEJKA_HH

template<typename TYP,unsigned int ROZMIAR>
class Kolejka{
  
  TYP* tablica[ROZMIAR];
  unsigned int ostatni;
  
public:
  Kolejka(){ostatni=0;}

  unsigned int rozmiar() const{return ostatni;}

  bool pusta() const {return ostatni==0;}
  
  void zamien(int w1,int w2){
    TYP *tmp=tablica[w1];
    tablica[w1]=tablica[w2];
    tablica[w2]=tmp;
    (tablica[w1])->kolejka=w1;
    (tablica[w2])->kolejka=w2;
  }
    
  void dodaj(TYP& w){
    tablica[++ostatni]=&w;
    wgore();
  }
  
    
  const TYP* zwroc_min() const{return tablica[1];}
  TYP* usun_min(){
    TYP* tmp=tablica[1];
    if(ostatni!=0){
      zamien(1,ostatni);
      --ostatni;
      wdol();
    }
    return tmp;
  }
  
  void wyswietl(){
    for(unsigned int i=1;i<=ostatni;++i){
      std::cout<<(tablica[i])->element<<" "<<(tablica[i])->droga<<"  ";
    }
    std::cout<<std::endl;
  }
  
  void po_zamianie_klucza(unsigned int Indeks){
    unsigned int i=Indeks/2,j=Indeks*2;
    if(Indeks>1){if(tablica[Indeks]->droga<tablica[i]->droga){wgore(Indeks);}}
    if(j<ostatni){if(tablica[Indeks]->droga>tablica[j]->droga||tablica[Indeks]->droga>tablica[j+1]->droga){wdol(Indeks);}}
    else if(j==ostatni){if(tablica[Indeks]->droga>tablica[j]->droga){wdol(Indeks);}}
  }
  
  void wgore(unsigned int i=0){
    if(i==0){i=ostatni;}
    unsigned int j;
    while(i>1)
      {
	j=i/2;
	if((tablica[i])->droga<(tablica[j])->droga){zamien(i,j);}
	else {break;}
	i=j;
      }
  }
  
  void wdol(unsigned int i=1)
  {
    unsigned int j;
    while(i<=ostatni/2)
      {
	j=2*i;
	if(j+1<=ostatni && (tablica[j+1])->droga<(tablica[j])->droga){++j;}
	if((tablica[i])->droga>(tablica[j])->droga){zamien(i,j);}
	else {break;}
	i=j;
      }
  }

};

#endif
