#ifndef MACIERZGRAF_HH
#define MACIERZGRAF_HH

#include "Lista.hh"
#include "Kolejka.hh"
#define oo 30000
#define MAX 1000
class MacierzGraf{
public:
  struct MWierzcholek;
  struct MKrawedz;
  struct MParaWierzcholkow;

  MacierzGraf();
  Lista<MKrawedz> KrawedzieGrafu()const;
  Lista<MWierzcholek> WierzcholkiGrafu()const;
  Lista< MKrawedz*> incydentneKrawedzie( const MWierzcholek& w)const;
  void dodajWierzcholek(int elem);
  void dodajWierzcholek(MWierzcholek &w);
  void dodajKrawedz(MWierzcholek &v,MWierzcholek& w,int elem);
  void dodajKrawedz(int w1_elem, int w2_elem, int wartosc);
  Lista<MWierzcholek>::Iterator znajdzWierzcholek(const MWierzcholek& w);
  MWierzcholek naprzeciwko(const MWierzcholek& w,const MKrawedz& k);
  void znajdz_sciezke(const MWierzcholek& start);
  bool szukajWierzcholka(const MWierzcholek& w);
  bool sasiedzi(const MWierzcholek& v,const MWierzcholek& w);
  MParaWierzcholkow konceKrawedzi(const MKrawedz& k);
  void usunKrawedz(MKrawedz& k);
  void usunWierzcholek(MWierzcholek& w);

private:
  Lista<MKrawedz> Lista_Krawedzi;
  Lista<MWierzcholek> Lista_Wierzcholkow;
  MKrawedz* Incydentne [MAX][MAX];
  int ostatni;
}; 

struct MacierzGraf::MWierzcholek{
  int element;
  int droga;
  unsigned int kolejka;
  int numer;
  Lista<int> trasa;
  MWierzcholek(int elem=0,int tania=0,unsigned int indeks=0,int liczba=0){element=elem;droga=tania;kolejka=indeks;numer=liczba;}
  bool operator == (const MWierzcholek& w)const{
    return w.element==element;
  }
  void ustaw_droge(MWierzcholek &w,MKrawedz &k);
  
};

struct MacierzGraf::MKrawedz{
  int waga;
  MWierzcholek* A;
  MWierzcholek* B;
  MKrawedz(MWierzcholek* poczatek=nullptr,MWierzcholek* koniec=nullptr, int wartosc=0){
    A=poczatek;
    B=koniec;
    waga=wartosc;
  }
  bool operator == (const MKrawedz& k)const{
    return A==k.A && B==k.B && waga==k.waga;
  }
  
};

struct MacierzGraf::MParaWierzcholkow{
  MWierzcholek A;
  MWierzcholek B;
  MParaWierzcholkow(MWierzcholek& w,MWierzcholek& v){A=w; B=v;}
};

#endif
