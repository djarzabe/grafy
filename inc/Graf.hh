#ifndef GRAF_HH
#define GRAF_HH

#define oo 30000
#include "Lista.hh"
#include "Kolejka.hh"
class Graf{
public:
  struct Wierzcholek;
  struct Krawedz;
  struct ParaWierzcholkow;

  bool sasiedzi(const Wierzcholek& v,const Wierzcholek& w);
  Wierzcholek naprzeciwko(const Wierzcholek& w,const Krawedz& k);
  ParaWierzcholkow konceKrawedzi(const Krawedz& k);
  void dodajKrawedz(Krawedz &k);
  void dodajKrawedz(Wierzcholek &v,Wierzcholek& w,int elem);
  void dodajKrawedz(int w1_elem, int w2_elem, int wartosc);
  void dodajWierzcholek(int elem);
  void dodajWierzcholek(const Wierzcholek &w);
  void nowyElement(Wierzcholek &w, int nowa_wartosc);
  void nowyElement(Krawedz &k, int nowa_wartosc);
  void usunKrawedz(Krawedz& k);
  void usunWierzcholek(Wierzcholek &w);
  Lista<Krawedz*> incydentneKrawedzie( const Wierzcholek& w)const;
  Lista<Krawedz> KrawedzieGrafu()const;
  Lista<Wierzcholek> WierzcholkiGrafu()const;
  bool szukajWierzcholka(const Wierzcholek& w);
  Lista<Krawedz>::Iterator znajdzKrawedz(const Krawedz& k);
  void znajdz_sciezke(const Wierzcholek& start);
  Lista<Wierzcholek>::Iterator znajdzWierzcholek(const Wierzcholek& w);
private:
  Lista<Krawedz> Lista_Krawedzi;
  Lista<Wierzcholek> Lista_Wierzcholkow;
}; 

struct Graf::Wierzcholek{
  int element;
  int droga;
  unsigned int kolejka;
  Lista<Krawedz*> incydentne;
  Lista<int> trasa;
  Wierzcholek(int elem=0,int tania=0,unsigned int indeks=0){element=elem;droga=tania;kolejka=indeks;}
  bool operator == (const Wierzcholek& w)const{
    return w.element==element;
  }
  void ustaw_droge(Wierzcholek& w,Krawedz& k);
  
};

struct Graf::Krawedz{
  int waga;
  Wierzcholek* A;
  Wierzcholek* B;
  Krawedz(Wierzcholek* poczatek=nullptr,Wierzcholek* koniec=nullptr, int wartosc=0){
    A=poczatek;
    B=koniec;
    waga=wartosc;
  }
  bool operator == (const Krawedz& k)const{
    return A==k.A && B==k.B && waga==k.waga;
  }
  
};
struct Graf::ParaWierzcholkow{
  Wierzcholek A;
  Wierzcholek B;
  ParaWierzcholkow(Wierzcholek &w,Wierzcholek &v){A=w; B=v;}
};



#endif
